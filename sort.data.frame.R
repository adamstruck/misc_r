##' Adds a method to sort for handling data.frames
##'
##' It sorts on the first column by default, but you may use any vector of valid column indices.
##' @title sort.data.frame
##' @param x data.frame object
##' @param decreasing 
##' @param by 
##' @param ... 
##' @return sorted data.frame
##' @author Adam Struck
sort.data.frame <- function(x, decreasing=FALSE, by=1, ... ){
    f <- function(...) order(...,decreasing=decreasing)
    i <- do.call(f,x[by])
    x[i,,drop=FALSE]
}
